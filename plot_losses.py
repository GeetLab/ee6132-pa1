import matplotlib.pyplot as plt
import numpy as np

train_losses = np.load('train_losses.npy')
test_losses = np.load('test_losses.npy')
train_iters = range(len(train_losses))
# since test loss is evaluated only after every 200 iterations
test_iters = 200*np.array(range(len(test_losses)))

plt.plot(train_iters,train_losses,'go-',label='Training')
plt.plot(test_iters,test_losses,'ro-',label='Test')
plt.ylabel('Loss')
plt.xlabel('Iteration')
plt.legend()
plt.show()
