## Programming Assignment : NN for MNIST classification

Classification of MNIST digits using multilayer perceptron, without using any Deep Learning libraries.
It implements forward pass and backpropagation, activation functions, etc.


