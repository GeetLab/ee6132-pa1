'''
Python 2 uses .npy in ASCII format by default, Python 3 uses utf-8.
Change accordingly for reading/writing
'''
import matplotlib.pyplot as plt
import numpy as np
from mnist import MNIST

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def reLU(x):
    num = len(np.extract(x<0,x))
    if num>0:
        zeroes = np.zeros(num)
        np.place(x,x<0,zeroes)
    return x

def activation(x,mode):
# 1- sigmoid, 2- ReLU
    if mode == 1:
        return sigmoid(x)
    elif mode == 2:
        return reLU(x)
def run_network(x,W,b,mode):
    z2 = np.matmul(W[0],x) + b[0] # a1 = z1 = x
    a2 = activation(z2,mode)
    z3 = np.matmul(W[1],a2) + b[1]
    a3 = activation(z3,mode)
    z4 = np.matmul(W[2],a3) + b[2]
    a4 = activation(z4,mode)
    z5 = np.matmul(W[3],a4) + b[3]
    e = np.exp(z5)
    c = e/sum(e)
    return c


mndata = MNIST('Data')
images_test, labels_test = mndata.load_testing()
X_test = np.array(images_test)
m = len(X_test)

print('Loading trained weights...')

W_sigmoid = np.load('sigm_weights.npy',encoding = 'latin1')
b_sigmoid = np.load('sigm_biases.npy',encoding = 'latin1')
W_ReLU = np.load('rl_weights.npy',encoding = 'latin1')
b_ReLU = np.load('rl_biases.npy',encoding = 'latin1')

# W_sigmoid = np.load('sigm_weights.npy')
# b_sigmoid = np.load('sigm_biases.npy')
# W_ReLU = np.load('rl_weights.npy')
# b_ReLU = np.load('rl_biases.npy')
print('Done loading.')

rand_ind = np.random.permutation(range(20))
fig = plt.figure()
for i in range(20):
    plt.subplot(4,5,i+1)
    plt.imshow(X_test[rand_ind[i]].reshape((28,28)))
    plt.axis('off')
    c_sigmoid = run_network(X_test[rand_ind[i]].reshape((784,1)),W_sigmoid,b_sigmoid,1)
    c_ReLU = run_network(X_test[rand_ind[i]].reshape((784,1)),W_ReLU,b_ReLU,2)
    print(str.format('Ground Truth: {0}',labels_test[rand_ind[i]]))
    print(str.format('Top predictions, Sigmoid : {0}',np.flip(np.argsort(c_sigmoid.reshape((10,))),axis=0)[0:3] ))
    print(str.format('Top predictions, ReLU : {0}',np.flip(np.argsort(c_ReLU.reshape((10,))),axis=0)[0:3] ))
plt.show()

sum_sigm = 0
sum_rl = 0
print('Computing accuracy')
for i in range(m):
    print(str.format('{0}/{1}',i+1,m)),
    c_sigmoid = run_network(X_test[i].reshape((784,1)),W_sigmoid,b_sigmoid,1)
    c_ReLU = run_network(X_test[i].reshape((784,1)),W_ReLU,b_ReLU,2)
    if np.argmax(c_sigmoid) == labels_test[i]:
        sum_sigm = sum_sigm + 1
    if np.argmax(c_ReLU) == labels_test[i]:
        sum_rl = sum_rl + 1

print(str.format('Sigmoid :{}, ReLU:{}',sum_sigm/m*100,sum_rl/m*100))
