'''
This program trains the MLP for MNIST classification
Modify parameters as needed. For selecting Sigmoid/ReLU, use the ACTIV_FN variable
Uncomment the final part, and type in desired ouput file names to save the weights after training

Python 2 uses .npy in ASCII format by default, Python 3 uses utf-8.
Change accordingly for reading/writing
'''

# Parameters
BATCH_SIZE = 64
LAMBDA = 0.005 # learning rate
MOMENTUM = 0.7 # for SGD
ITERS = 8000
INIT_STDDEV = 0.08
GAMMA = 1 # learning rate decay
ACTIV_FN = 2 # 1 - sigmoid, 2- reLU
DECAY_DELAY = 250
TEST_DELAY = 200
epsilon = 0.001

from mnist import MNIST
import numpy as np

def create_onehot(labels):
    m = len(labels)
    y = np.zeros((m,10))
    rows = tuple(range(m))
    cols = tuple(labels)
    y[rows,cols] = 1
    return y

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def reLU(x):
    num = len(np.extract(x<0,x))
    if num>0:
        zeroes = np.zeros(num)
        np.place(x,x<0,zeroes)
    return x

def activation(x):
    if ACTIV_FN == 1:
        return sigmoid(x)
    elif ACTIV_FN == 2:
        return reLU(x)

def activation_(x):
    if ACTIV_FN == 1:
        return sigmoid(x)*(1-sigmoid(x))
    elif ACTIV_FN == 2:
        d_relu = np.ones(np.shape(x))
        np.place(d_relu,x<=0,np.zeros(len(np.extract(x<=0,x))))
        return d_relu

def mat_sq_sum(M):
    return sum(sum(M*M))

def zero_weights():
    return np.array([np.zeros((1000,784)),np.zeros((500,1000)),np.zeros((250,500)),np.zeros((10,250))])

def zero_biases():
    return np.array([np.zeros((1000,1)),np.zeros((500,1)),np.zeros((250,1)),np.zeros((10,1))])

def forward_backward_pass(x,y,W,b,mode):
    # mode: 1 = train, 2 = test

    # forward pass
    z2 = np.matmul(W[0],x) + b[0] # a1 = z1 = x
    a2 = activation(z2)

    z3 = np.matmul(W[1],a2) + b[1]
    a3 = activation(z3)

    z4 = np.matmul(W[2],a3) + b[2]
    a4 = activation(z4)

    z5 = np.matmul(W[3],a4) + b[3]
    subtrahend = max(z5)
    a5 = z5 - subtrahend
    # output activation = linear,but to avoid overflow/underflow, subtract max value from each output

    # J = sum(-y*(a5 - np.log(sum(np.exp(a5))))) + LAMBDA*(mat_sq_sum(W[0]) + mat_sq_sum(W[1]) + mat_sq_sum(W[2]) + mat_sq_sum(W[3]))/2
    # To save calculations, the regularization part is added once after the minibatch_size iterations
    J = sum(-y*(a5 - np.log(sum(np.exp(a5)))))

    if mode == 2:
        return J

    # backward propagation
    W_grad = np.empty([4,],dtype=object)

    delta_5 = - y + np.exp(a5)/sum(np.exp(a5))

    # delta_l = ( (W_l)'*delta_l+1 ) o f'(z_l)
    delta_4 = np.matmul(np.transpose(W[3]),delta_5)*activation_(z4)
    delta_3 = np.matmul(np.transpose(W[2]),delta_4)*activation_(z3)
    delta_2 = np.matmul(np.transpose(W[1]),delta_3)*activation_(z2)

    b_grad = np.array([delta_2,delta_3,delta_4,delta_5])
    # again, regularization part added after every batch, hence not returned for here
    W_grad[0] = np.matmul(delta_2,np.transpose(x))
    W_grad[1] = np.matmul(delta_3,np.transpose(a2))
    W_grad[2] = np.matmul(delta_4,np.transpose(a3))
    W_grad[3] = np.matmul(delta_5,np.transpose(a4))

    return [J,W_grad,b_grad]

mndata = MNIST('Data')
# Location where MNIST data is stored
# rename files; replace '.' with '-', that's how the library likes it!
images_train, labels_train = mndata.load_training()
images_test, labels_test = mndata.load_testing()

X_train = np.array(images_train)
y_train = create_onehot(labels_train)
X_test = np.array(images_test)
y_test = create_onehot(labels_test)
m = len(X_train)
m_test = len(X_test)

# velocities for momentum SGD
W_v = zero_weights()
b_v = zero_biases()

# initializing the weights and biases
W = np.empty([4,],dtype=object)
W[0] = np.random.normal(0,INIT_STDDEV,1000*784).reshape((1000,784))
W[1] = np.random.normal(0,INIT_STDDEV,500*1000).reshape((500,1000))
W[2] = np.random.normal(0,INIT_STDDEV,250*500).reshape((250,500))
W[3] = np.random.normal(0,INIT_STDDEV,10*250).reshape((10,250))
#special initialization for ReLU
if ACTIV_FN == 2:
    sizes = [784,1000,500,250,10]
    for i in range(4):
        for j in range(sizes[i+1]):
            W[i][j,:] = np.random.randn(sizes[i])*np.sqrt(2.0/sizes[i])

b = np.empty([4,],dtype=object)
b[0] = np.random.normal(0,INIT_STDDEV,1000).reshape((1000,1))
b[1] = np.random.normal(0,INIT_STDDEV,500).reshape((500,1))
b[2] = np.random.normal(0,INIT_STDDEV,250).reshape((250,1))
b[3] = np.random.normal(0,INIT_STDDEV,10).reshape((10,1))

train_losses,test_losses = [],[]
iter_num = 0
while iter_num<ITERS:
    rand_ind = np.random.permutation(range(m))
    batch_counter = 0
    batch_loss = 0
    W_grad = zero_weights()
    b_grad = zero_biases()
    for i in range(m):
        if iter_num == ITERS:
            break

        [J,Wg,bg] = forward_backward_pass(X_train[rand_ind[i]].reshape(784,1),y_train[rand_ind[i]].reshape(10,1),W,b,1)

        # summing up the gradients
        W_grad = W_grad + Wg
        b_grad = b_grad + bg
        batch_counter = batch_counter + 1
        batch_loss = batch_loss + J
        if batch_counter == BATCH_SIZE  or i == m-1:
            batch_loss = batch_loss/batch_counter + LAMBDA*(mat_sq_sum(W[0]) + mat_sq_sum(W[1]) + mat_sq_sum(W[2]) + mat_sq_sum(W[3]))/2
            print(str.format('Iteration {0}, {1}/{2}, Loss :{3}',iter_num,i,m,batch_loss))
            train_losses.append(batch_loss)
            # updating velocity
            W_grad = W_grad/batch_counter + np.array([LAMBDA*W[0],LAMBDA*W[1],LAMBDA*W[2],LAMBDA*W[3]],dtype=object)
            W_v = MOMENTUM*W_v - epsilon*W_grad
            b_v = MOMENTUM*b_v - epsilon*b_grad/batch_counter

            # applying the update
            W = W + W_v
            b = b + b_v

            if iter_num%DECAY_DELAY == 0:
                epsilon = epsilon * GAMMA
            if iter_num%TEST_DELAY == 0:
                test_loss = 0
                for i in range(m_test):
                    test_loss = test_loss + forward_backward_pass(X_test[i].reshape(784,1),y_test[i].reshape(10,1),W,b,2)
                test_losses.append(test_loss/m_test)
                print(str.format('Average loss over test data: {0}',test_loss/m_test))

            batch_counter = 0
            batch_loss = 0
            iter_num = iter_num + 1
            W_grad = zero_weights()
            b_grad = zero_biases()

# np.save('ReLU_nd_0_001_weights.npy',W)
# np.save('ReLU_nd_0_001_biases.npy',b)
# np.save('ReLU_nd_0_001_train_losses.npy',np.array(train_losses))
# np.save('ReLU_nd_0_001_test_losses.npy',np.array(test_losses))
